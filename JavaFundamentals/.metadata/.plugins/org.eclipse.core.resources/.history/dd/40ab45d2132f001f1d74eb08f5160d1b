package com.gl.eventsApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.eventsApp.beans.Events;
import com.gl.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.service.EventsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/events")
public class EventsController {

    @Autowired
    EventsService eventsService;

    @GetMapping("/allEvents")
    public ResponseEntity<?> getAllEvents() {
        try {
            List<Events> events = eventsService.getAllEvents();
            if (events.isEmpty()) {
                return new ResponseEntity<>("No events found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(events, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to fetch events: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/allSpecialEvents")
    public ResponseEntity<?> getAllSpecialEvents() {
        try {
            List<SpecialEvents> specialEvents = eventsService.getAllSpecialEvents();
            if (specialEvents.isEmpty()) {
                return new ResponseEntity<>("No special events found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(specialEvents, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to fetch special events: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/events")
    public ResponseEntity<?> addEvent(@RequestBody Events event) {
        try {
            Events addedEvent = eventsService.addEvent(event);
            return new ResponseEntity<>(addedEvent, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to add event: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/events/{id}")
    public ResponseEntity<?> updateEvent(@PathVariable Integer id, @RequestBody Events event) {
        try {
            Events updatedEvent = eventsService.updateEvent(id, event);
            return new ResponseEntity<>(updatedEvent, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to update event: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/events/{id}")
    public ResponseEntity<?> deleteEvent(@PathVariable Integer id) {
        try {
            eventsService.deleteEvent(id);
            return new ResponseEntity<>("Event deleted successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to delete event: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

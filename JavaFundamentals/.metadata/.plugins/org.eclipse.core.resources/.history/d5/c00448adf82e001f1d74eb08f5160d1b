package com.slk.eventsApp.controllerIntegrationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slk.eventsApp.beans.Events;
import com.slk.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.beans.User;
import com.slk.eventsApp.controller.UserController;
import com.slk.eventsApp.service.EventsService;
import com.slk.eventsApp.service.UserService;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private UserService userService;

	@Test
	public void testRegisterUser_Success() throws Exception {
		// Prepare a user object
		User user = new User("username", "password");

		// Mock the service response
		when(userService.addUser(any(User.class))).thenReturn(user);

		// Perform the POST request
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();

		// Verify the response
		String responseBody = result.getResponse().getContentAsString();
		assertEquals("registeration successful!", responseBody);
	}

	@Test
	public void testRegisterUser_ServiceException() throws Exception {
		// Prepare a user object
		User user = new User("username", "password");

		// Mock the service to throw an exception
		when(userService.addUser(any(User.class))).thenThrow(new RuntimeException("Failed to register user"));

		// Perform the POST request
		mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user)))
		.andExpect(MockMvcResultMatchers.status().isInternalServerError())
		.andExpect(MockMvcResultMatchers.content().string("registeration failed!"));
	}

	@Test
	public void testLoginUser_Success() throws Exception {
		// Prepare a user object
		User user = new User("username", "password"); // Assuming login does not return sensitive data

		// Mock the service response
		when(userService.findUser(any(User.class))).thenReturn(user);

		// Perform the POST request
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		// Verify the response
		String responseBody = result.getResponse().getContentAsString();
		assertEquals("login successful!", responseBody);
	}

	@Test
	public void testLoginUser_UserNotFound() throws Exception {
		// Prepare a user object
		User user = new User("username", "password");

		// Mock the service to return null, indicating user not found
		when(userService.findUser(any(User.class))).thenReturn(null);

		// Perform the POST request
		mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user)))
		.andExpect(MockMvcResultMatchers.status().isNotFound())
		.andExpect(MockMvcResultMatchers.content().string("login failed!"));
	}

	@Test
	public void testLoginUser_ServiceException() throws Exception {
		// Prepare a user object
		User user = new User("username", "password");

		// Mock the service to throw an exception
		when(userService.findUser(any(User.class))).thenThrow(new RuntimeException("Failed to login user"));

		// Perform the POST request
		mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(user)))
		.andExpect(MockMvcResultMatchers.status().isInternalServerError())
		.andExpect(MockMvcResultMatchers.content().string(""));
	}
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;


	@MockBean
	private EventsService eventsService;

	private String baseUrl;

	@BeforeEach
	public void setUp() {
		baseUrl = "http://localhost:" + port + "/events";
	}

	@Test
	public void testGetAllEvents_Success() throws Exception {
		// Mock data
		List<Events> mockEvents = List.of(
				new Events(1, "Event 1", "Description 1", "2024-06-21"),
				new Events(2, "Event 2", "Description 2", "2024-06-22")
				);

		// Mock service method
		when(eventsService.getAllEvents()).thenReturn(mockEvents);

		// Make GET request
		ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allEvents", HttpMethod.GET, null, String.class);

		// Verify response
		assertEquals(HttpStatus.OK, response.getStatusCode());

		// Deserialize response body
		List<Events> eventsList = objectMapper.readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(List.class, Events.class));

		// Assert that the returned list matches the mock data
		assertEquals(mockEvents.size(), eventsList.size());
		assertEquals(mockEvents.get(0).getName(), eventsList.get(0).getName());
		assertEquals(mockEvents.get(1).getDescription(), eventsList.get(1).getDescription());

		// Verify that the service method was called once
		verify(eventsService, times(1)).getAllEvents();
	}

	//    @Test
	//    public void testGetAllEvents_Empty() throws Exception {
	//        // Mock service method to return empty list
	//        when(eventsService.getAllEvents()).thenReturn(Collections.emptyList());
	//
	//        // Make GET request
	//        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allEvents", HttpMethod.GET, null, String.class);
	//
	//        // Verify response
	//        assertEquals(HttpStatus.OK, response.getStatusCode());
	//
	//        // Deserialize response body
	//        List<Events> eventsList = objectMapper.readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(List.class, Events.class));
	//
	//        // Assert that the returned list is empty
	//        assertEquals(0, eventsList.size());
	//
	//        // Verify that the service method was called once
	//        verify(eventsService, times(1)).getAllEvents();
	//    }

	@Test
	public void testGetAllEvents_ErrorHandling() {
		// Simulate an error scenario in the service layer
		when(eventsService.getAllEvents()).thenThrow(RuntimeException.class);

		// Make GET request
		ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allEvents", HttpMethod.GET, null, String.class);

		// Verify response
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testGetAllSpecialEvents_Success() throws Exception {
		// Mock data
		List<SpecialEvents> mockSpecialEvents = List.of(
				new SpecialEvents(1, "Special Event 1", "Special Description 1", "2024-06-21"),
				new SpecialEvents(2, "Special Event 2", "Special Description 2", "2024-06-22")
				);

		// Mock service method
		when(eventsService.getAllSpecialEvents()).thenReturn(mockSpecialEvents);

		// Make GET request
		ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allSpecialEvents", HttpMethod.GET, null, String.class);

		// Verify response
		assertEquals(HttpStatus.OK, response.getStatusCode());

		// Deserialize response body
		List<SpecialEvents> specialEventsList = objectMapper.readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(List.class, SpecialEvents.class));

		// Assert that the returned list matches the mock data
		assertEquals(mockSpecialEvents.size(), specialEventsList.size());
		assertEquals(mockSpecialEvents.get(0).getName(), specialEventsList.get(0).getName());
		assertEquals(mockSpecialEvents.get(1).getDescription(), specialEventsList.get(1).getDescription());

		// Verify that the service method was called once
		verify(eventsService, times(1)).getAllSpecialEvents();
	}

	//    @Test
	//    public void testGetAllSpecialEvents_Empty() throws Exception {
	//        // Mock service method to return empty list
	//        when(eventsService.getAllSpecialEvents()).thenReturn(Collections.emptyList());
	//
	//        // Make GET request
	//        ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allSpecialEvents", HttpMethod.GET, null, String.class);
	//
	//        // Verify response
	//        assertEquals(HttpStatus.OK, response.getStatusCode());
	//
	//        // Deserialize response body
	//        List<SpecialEvents> specialEventsList = objectMapper.readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(List.class, SpecialEvents.class));
	//
	//        // Assert that the returned list is empty
	//        assertEquals(0, specialEventsList.size());
	//
	//        // Verify that the service method was called once
	//        verify(eventsService, times(1)).getAllSpecialEvents();
	//    }

	@Test
	public void testGetAllSpecialEvents_ErrorHandling() {
		// Simulate an error scenario in the service layer
		when(eventsService.getAllSpecialEvents()).thenThrow(RuntimeException.class);

		// Make GET request
		ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/allSpecialEvents", HttpMethod.GET, null, String.class);

		// Verify response
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}
}

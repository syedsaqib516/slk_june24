package strings;
import java.util.Scanner;

public class TextAnalysisTool {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] sentences = new String[1000];
        String[] words = new String[10000];
        int sentenceCount = 0;
        int wordCount = 0;

        // Reading sentences
        while (true) {
            String sentence = scanner.nextLine().trim();
            if (sentence.isEmpty()) {
                break;
            }
            sentences[sentenceCount++] = sentence;
            String[] sentenceWords = sentence.split("\\s+");
            for (String word : sentenceWords) {
                words[wordCount++] = word.replaceAll("\\.", "").toLowerCase();
            }
        }
        scanner.close();

        // Counting unique words and frequencies
        String[] uniqueWords = new String[wordCount];
        int[] wordFrequencies = new int[wordCount];
        int uniqueWordCount = 0;

        for (int i = 0; i < wordCount; i++) {
            String word = words[i];
            boolean found = false;
            for (int j = 0; j < uniqueWordCount; j++) {
                if (uniqueWords[j].equals(word)) {
                    wordFrequencies[j]++;
                    found = true;
                    break;
                }
            }
            if (!found) {
                uniqueWords[uniqueWordCount] = word;
                wordFrequencies[uniqueWordCount] = 1;
                uniqueWordCount++;
            }
        }

        // Sorting unique words
        for (int i = 0; i < uniqueWordCount - 1; i++) {
            for (int j = i + 1; j < uniqueWordCount; j++) {
                if (uniqueWords[i].compareTo(uniqueWords[j]) > 0) {
                    String tempWord = uniqueWords[i];
                    uniqueWords[i] = uniqueWords[j];
                    uniqueWords[j] = tempWord;

                    int tempFreq = wordFrequencies[i];
                    wordFrequencies[i] = wordFrequencies[j];
                    wordFrequencies[j] = tempFreq;
                }
            }
        }

        // Finding the most frequent word
        String mostFrequentWord = uniqueWords[0];
        int maxFrequency = wordFrequencies[0];
        for (int i = 1; i < uniqueWordCount; i++) {
            if (wordFrequencies[i] > maxFrequency) {
                mostFrequentWord = uniqueWords[i];
                maxFrequency = wordFrequencies[i];
            }
        }

        // Output results
        System.out.println("Number of sentences: " + sentenceCount);
        System.out.println("Total number of words: " + wordCount);
        System.out.printf("Average number of words per sentence: %.2f%n", (double) wordCount / sentenceCount);
        System.out.print("Unique words: ");
        for (int i = 0; i < uniqueWordCount; i++) {
            if (i > 0) System.out.print(", ");
            System.out.print(uniqueWords[i]);
        }
        System.out.println();
        System.out.println("Most frequent word: " + mostFrequentWord + " (" + maxFrequency + " times)");

        // Printing sentences in reverse order
        System.out.println("Sentences in reverse order:");
        for (int i = sentenceCount - 1; i >= 0; i--) {
            System.out.println(sentences[i]);
        }
    }
}

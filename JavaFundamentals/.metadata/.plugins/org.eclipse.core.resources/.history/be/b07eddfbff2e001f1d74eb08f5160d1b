package com.slk.eventsApp.controllerIntegrationTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.slk.eventsApp.beans.User;
import com.slk.eventsApp.repo.UserRepo;
import com.slk.eventsApp.service.UserService;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepo userRepo;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setup() {
        // Mock behavior of userRepo.save()
        when(userRepo.save(any(User.class))).thenAnswer(invocation -> {
            User user = invocation.getArgument(0);
            user.setUserName("test"); // Simulating the save operation setting the ID
            return user;
        });

        // Mock behavior of userRepo.findByUserNameAndPassword()
        when(userRepo.findByUserNameAndPassword(any(String.class), any(String.class))).thenReturn(new User("testUser", "password"));
    }

    @Test
    public void testAddUser_Success() {
        User newUser = new User("testUser", "password");

        User addedUser = userService.addUser(newUser);

        assertNotNull(addedUser);
        assertEquals("testUser", addedUser.getUserName());
        assertEquals("password", addedUser.getPassword());

        // Verify that save() method of userRepo was called exactly once
        verify(userRepo, times(1)).save(any(User.class));
    }

    @Test
    public void testFindUser_Success() {
        User userToFind = new User("testUser", "password");

        User foundUser = userService.findUser(userToFind);

        assertNotNull(foundUser);
        assertEquals("testUser", foundUser.getUserName());
        assertEquals("password", foundUser.getPassword());

        // Verify that findByUserNameAndPassword() method of userRepo was called exactly once
        verify(userRepo, times(1)).findByUserNameAndPassword(any(String.class), any(String.class));
    }
}

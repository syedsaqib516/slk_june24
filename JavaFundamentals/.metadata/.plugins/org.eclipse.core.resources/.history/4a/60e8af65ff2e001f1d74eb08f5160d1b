package com.slk.eventsApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.slk.eventsApp.beans.Events;
import com.slk.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.repo.EventsRepo;
import com.slk.eventsApp.repo.SpecialEventsRepo;

@Service
public class EventsService 
{
	@Autowired
	EventsRepo eventsRepo;

	@Autowired
	SpecialEventsRepo specialEventsRepo;


	 public List<Events> getAllEvents() {
	        try {
	            return eventsRepo.findAll();
	        } catch (DataAccessException e) {
	            // Log the exception or rethrow as needed
	            throw new RuntimeException("Failed to fetch events from the database", e);
	        }
	    }

	    public Events addEvent(Events event) {
	        try {
	            return eventsRepo.save(event);
	        } catch (DataAccessException e) {
	            // Log the exception or rethrow as needed
	            throw new RuntimeException("Failed to add event to the database", e);
	        }
	    }

	    public List<SpecialEvents> getAllSpecialEvents() {
	        try {
	            return specialEventsRepo.findAll();
	        } catch (DataAccessException e) {
	            // Log the exception or rethrow as needed
	            throw new RuntimeException("Failed to fetch special events from the database", e);
	        }
	    }
	public Events updateEvent(Integer id, Events updatedEvent) {
		Optional<Events> existingEvent = eventsRepo.findById(id);
		if (existingEvent.isPresent()) {
			updatedEvent.setEventId(id);
			return eventsRepo.save(updatedEvent);
		} else {
			throw new RuntimeException("Event not found with id: " + id);
		}
	}

	public void deleteEvent(Integer id) {
		Optional<Events> eventToDelete = eventsRepo.findById(id);
		if (eventToDelete.isPresent()) {
			eventsRepo.delete(eventToDelete.get());
		} else {
			throw new RuntimeException("Event not found with id: " + id);
		}
	}
}

package com.slk.eventsApp.controllerIntegrationTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.hibernate.service.spi.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slk.eventsApp.beans.User;
import com.slk.eventsApp.service.UserService;

//@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    public void testRegisterUser_Success() throws Exception {
        // Mock the service response
        User user = new User("username", "password", "email");
        when(userService.addUser(any(User.class))).thenReturn(user);

        // Perform the POST request
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        // Verify the response
        String responseBody = result.getResponse().getContentAsString();
        User responseUser = objectMapper.readValue(responseBody, User.class);
        assertEquals(user.getUsername(), responseUser.getUsername());
        // Add more assertions as needed
    }

    @Test
    public void testRegisterUser_ServiceException() throws Exception {
        // Mock the service to throw an exception
        when(userService.addUser(any(User.class))).thenThrow(new ServiceException("Failed to register user"));

        // Prepare a user object
        User user = new User("username", "password", "email");

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string(""));
                // You can verify specific error messages or handle responses accordingly
    }

    @Test
    public void testLoginUser_Success() throws Exception {
        // Mock the service response
        User user = new User("username", "password", null); // Assuming login does not return sensitive data
        when(userService.findUser(any(User.class))).thenReturn(user);

        // Prepare a user object
        User loginUser = new User("username", "password", null);

        // Perform the POST request
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginUser)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // Verify the response
        String responseBody = result.getResponse().getContentAsString();
        User responseUser = objectMapper.readValue(responseBody, User.class);
        assertEquals(user.getUsername(), responseUser.getUsername());
        // Add more assertions as needed
    }

    @Test
    public void testLoginUser_UserNotFound() throws Exception {
        // Mock the service to return null, indicating user not found
        when(userService.findUser(any(User.class))).thenReturn(null);

        // Prepare a user object
        User loginUser = new User("username", "password", null);

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginUser)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string(""));
                // You can verify specific error messages or handle responses accordingly
    }

    @Test
    public void testLoginUser_ServiceException() throws Exception {
        // Mock the service to throw an exception
        when(userService.findUser(any(User.class))).thenThrow(new ServiceException("Failed to login user"));

        // Prepare a user object
        User loginUser = new User("username", "password", null);

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginUser)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string(""));
                // You can verify specific error messages or handle responses accordingly
    }
}
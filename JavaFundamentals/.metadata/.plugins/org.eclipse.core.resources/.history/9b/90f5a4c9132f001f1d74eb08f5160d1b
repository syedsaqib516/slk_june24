package com.slk.eventsApp.controllerIntegrationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.eventsApp.beans.User;
import com.slk.eventsApp.controller.UserController;
import com.slk.eventsApp.service.UserService;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    public void testRegisterUser_Success() throws Exception {
        // Prepare a user object
        User user = new User("username", "password");

        // Mock the service response
        when(userService.addUser(any(User.class))).thenReturn(user);

        // Perform the POST request
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();

        // Verify the response
        String responseBody = result.getResponse().getContentAsString();
        assertEquals("registeration successful!", responseBody);
    }

    @Test
    public void testRegisterUser_ServiceException() throws Exception {
        // Prepare a user object
        User user = new User("username", "password");

        // Mock the service to throw an exception
        when(userService.addUser(any(User.class))).thenThrow(new RuntimeException("Failed to register user"));

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string("registeration failed!"));
    }

    @Test
    public void testLoginUser_Success() throws Exception {
        // Prepare a user object
        User user = new User("username", "password"); // Assuming login does not return sensitive data

        // Mock the service response
        when(userService.findUser(any(User.class))).thenReturn(user);

        // Perform the POST request
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // Verify the response
        String responseBody = result.getResponse().getContentAsString();
        assertEquals("login successful!", responseBody);
    }

    @Test
    public void testLoginUser_UserNotFound() throws Exception {
        // Prepare a user object
        User user = new User("username", "password");

        // Mock the service to return null, indicating user not found
        when(userService.findUser(any(User.class))).thenReturn(null);

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("login failed!"));
    }

    @Test
    public void testLoginUser_ServiceException() throws Exception {
        // Prepare a user object
        User user = new User("username", "password");

        // Mock the service to throw an exception
        when(userService.findUser(any(User.class))).thenThrow(new RuntimeException("Failed to login user"));

        // Perform the POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().string(""));
    }
}

package com.slk.eventsApp.controllerIntegrationTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gl.eventsApp.beans.Events;
import com.gl.eventsApp.beans.SpecialEvents;
import com.slk.eventsApp.repo.EventsRepo;
import com.slk.eventsApp.repo.SpecialEventsRepo;
import com.slk.eventsApp.service.EventsService;

@ExtendWith(MockitoExtension.class)
public class EventsServiceTest {

    @Mock
    private EventsRepo eventsRepo;

    @Mock
    private SpecialEventsRepo specialEventsRepo;

    @InjectMocks
    private EventsService eventsService;

    private List<Events> mockEventsList;
    private List<SpecialEvents> mockSpecialEventsList;

    @BeforeEach
    public void setup() {
        // Initialize mock data
        mockEventsList = new ArrayList<>();
        mockEventsList.add(new Events(1, "Event 1", "Description 1", "2024-06-21"));
        mockEventsList.add(new Events(2, "Event 2", "Description 2", "2024-06-22"));

        mockSpecialEventsList = new ArrayList<>();
        mockSpecialEventsList.add(new SpecialEvents(101, "Special Event 1", "Special Description 1", "2024-06-21"));
        mockSpecialEventsList.add(new SpecialEvents(102, "Special Event 2", "Special Description 2", "2024-06-22"));
    }

    @Test
    public void testGetAllEvents_Success() {
        // Mock behavior of eventsRepo.findAll()
        when(eventsRepo.findAll()).thenReturn(mockEventsList);

        List<Events> events = eventsService.getAllEvents();

        assertEquals(2, events.size());
        assertEquals("Event 1", events.get(0).getName());
        assertEquals("Event 2", events.get(1).getName());

        // Verify that findAll() method of eventsRepo was called exactly once
        verify(eventsRepo, times(1)).findAll();
    }

    @Test
    public void testGetAllEvents_EmptyList() {
        // Mock behavior of eventsRepo.findAll()
        when(eventsRepo.findAll()).thenReturn(new ArrayList<>());

        List<Events> events = eventsService.getAllEvents();

        assertTrue(events.isEmpty());

        // Verify that findAll() method of eventsRepo was called exactly once
        verify(eventsRepo, times(1)).findAll();
    }

    @Test
    public void testGetAllSpecialEvents_Success() {
        // Mock behavior of specialEventsRepo.findAll()
        when(specialEventsRepo.findAll()).thenReturn(mockSpecialEventsList);

        List<SpecialEvents> specialEvents = eventsService.getAllSpecialEvents();

        assertEquals(2, specialEvents.size());
        assertEquals("Special Event 1", specialEvents.get(0).getName());
        assertEquals("Special Event 2", specialEvents.get(1).getName());

        // Verify that findAll() method of specialEventsRepo was called exactly once
        verify(specialEventsRepo, times(1)).findAll();
    }

    @Test
    public void testGetAllSpecialEvents_EmptyList() {
        // Mock behavior of specialEventsRepo.findAll()
        when(specialEventsRepo.findAll()).thenReturn(new ArrayList<>());

        List<SpecialEvents> specialEvents = eventsService.getAllSpecialEvents();

        assertTrue(specialEvents.isEmpty());

        // Verify that findAll() method of specialEventsRepo was called exactly once
        verify(specialEventsRepo, times(1)).findAll();
    }

    @Test
    public void testAddEvent_Success() {
        Events newEvent = new Events(null, "New Event", "New Event Description", "2024-06-23");
        Events savedEvent = new Events(1, "New Event", "New Event Description", "2024-06-23");

        // Mock behavior of eventsRepo.save()
        when(eventsRepo.save(any(Events.class))).thenReturn(savedEvent);

        Events addedEvent = eventsService.addEvent(newEvent);

        assertNotNull(addedEvent);
        assertEquals(1, addedEvent.getEventId());
        assertEquals("New Event", addedEvent.getName());

        // Verify that save() method of eventsRepo was called exactly once
        verify(eventsRepo, times(1)).save(any(Events.class));
    }

    @Test
    public void testUpdateEvent_Success() {
        Integer eventId = 1;
        Events updatedEvent = new Events(eventId, "Updated Event", "Updated Description", "2024-06-24");

        // Mock behavior of eventsRepo.findById() and eventsRepo.save()
        when(eventsRepo.findById(eventId)).thenReturn(Optional.of(mockEventsList.get(0)));
        when(eventsRepo.save(any(Events.class))).thenReturn(updatedEvent);

        Events result = eventsService.updateEvent(eventId, updatedEvent);

        assertNotNull(result);
        assertEquals(eventId, result.getEventId());
        assertEquals("Updated Event", result.getName());

        // Verify that findById() and save() methods of eventsRepo were called exactly once
        verify(eventsRepo, times(1)).findById(eventId);
        verify(eventsRepo, times(1)).save(any(Events.class));
    }

    @Test
    public void testUpdateEvent_NotFound() {
        Integer eventId = 10;
        Events updatedEvent = new Events(eventId, "Updated Event", "Updated Description", "2024-06-24");

        // Mock behavior of eventsRepo.findById()
        when(eventsRepo.findById(eventId)).thenReturn(Optional.empty());

        // Perform the update and expect RuntimeException
        assertThrows(RuntimeException.class, () -> eventsService.updateEvent(eventId, updatedEvent));

        // Verify that findById() method of eventsRepo was called exactly once
        verify(eventsRepo, times(1)).findById(eventId);
        // Verify that save() method of eventsRepo was not called
        verify(eventsRepo, never()).save(any(Events.class));
    }

    @Test
    public void testDeleteEvent_Success() {
        Integer eventId = 1;
        Events eventToDelete = new Events(eventId, "Event to delete", "Description", "2024-06-25");

        // Mock behavior of eventsRepo.findById() and eventsRepo.delete()
        when(eventsRepo.findById(eventId)).thenReturn(Optional.of(eventToDelete));

        eventsService.deleteEvent(eventId);

        // Verify that findById() and delete() methods of eventsRepo were called exactly once
        verify(eventsRepo, times(1)).findById(eventId);
        verify(eventsRepo, times(1)).delete(eventToDelete);
    }

    @Test
    public void testDeleteEvent_NotFound() {
        Integer eventId = 10;

        // Mock behavior of eventsRepo.findById()
        when(eventsRepo.findById(eventId)).thenReturn(Optional.empty());

        // Perform the delete and expect RuntimeException
        assertThrows(RuntimeException.class, () -> eventsService.deleteEvent(eventId));

        // Verify that findById() method of eventsRepo was called exactly once
        verify(eventsRepo, times(1)).findById(eventId);
        // Verify that delete() method of eventsRepo was not called
        verify(eventsRepo, never()).delete(any(Events.class));
    }
}

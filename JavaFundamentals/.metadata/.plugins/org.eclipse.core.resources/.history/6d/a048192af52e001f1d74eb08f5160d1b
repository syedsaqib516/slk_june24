package com.slk.eventsApp.controllerIntegrationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slk.eventsApp.beans.Events;
import com.slk.eventsApp.controller.EventsController;
import com.slk.eventsApp.service.EventsService;

@WebMvcTest(EventsController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EventsControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private EventsService eventsService;

	@Test
	public void testGetAllEvents_Success() throws Exception {
		// Mock the service response
		List<Events> eventsList = Arrays.asList(
				new Events(1, "Event 1", "Description 1", "2024-06-21"),
				new Events(2, "Event 2", "Description 2", "2024-06-22")
				);
		when(eventsService.getAllEvents()).thenReturn(eventsList);

		// Perform the GET request
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/events/allEvents")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		// Verify the response
		String responseBody = result.getResponse().getContentAsString();
		List<Events> responseEvents = Arrays.asList(objectMapper.readValue(responseBody, Events[].class));
		assertEquals(eventsList.size(), responseEvents.size());
		// Add more assertions as needed
	}

	@Test
	public void testGetAllEvents_ServiceException() throws Exception {
		// Mock the service to throw an exception
		when(eventsService.getAllEvents()).thenThrow(new ServiceException("Failed to retrieve events"));

		// Perform the GET request
		mockMvc.perform(MockMvcRequestBuilders.get("/events/allEvents")
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isInternalServerError())
		.andExpect(MockMvcResultMatchers.content().string(""));
		// You can verify specific error messages or handle responses accordingly
	}

	// Similar tests for getAllSpecialEvents can be added
}
